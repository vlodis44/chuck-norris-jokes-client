import React from 'react';
import {JokeDto} from 'dto/joke.dto';
import {useDispatch, useSelector} from 'react-redux';
import {addToFav, removeFromFav} from 'reducer';
import {RootState} from 'store';
import SingleJoke from './single-joke.component';

interface Props {
    onSidebar?: boolean;
}

const Jokes: React.FC<Props> = ({onSidebar}: Props) => {
    const dispatch = useDispatch();
    const {receivedJokes, favouriteJokes} = useSelector((state: RootState) => state);

    const requireHeart = (isFilled: boolean) => {
        return isFilled ? require('assets/img/filled-heart.svg') : require('assets/img/heart.svg');
    };

    const onHeartHover = (isInFav: boolean) => (e: React.MouseEvent<HTMLImageElement>) => {
        e.currentTarget.src = requireHeart(isInFav);
    };

    const onHeartClick = (isInFav: boolean, joke: JokeDto) => () => {
        dispatch(isInFav ? removeFromFav(joke) : addToFav(joke));
    };

    const jokesDiv = (onSidebar ? favouriteJokes : receivedJokes)
        .map((joke: JokeDto) => {
            const isInFav = favouriteJokes.includes(joke);

            return (
                <SingleJoke key={joke.id + onSidebar?.toString()}
                    {...{joke, onSidebar, isInFav, requireHeart, onHeartHover, onHeartClick}}/>
            );
        });

    return (
        <>
            {jokesDiv.reverse()}
        </>
    );
};

export default Jokes;
