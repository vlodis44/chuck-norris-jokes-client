import React, {useEffect, useState} from 'react';
import CSSTransition from 'react-transition-group/CSSTransition';
import {JokeDto} from '../../dto/joke.dto';

interface Props {
    joke: JokeDto;
    onSidebar?: boolean;
    isInFav: boolean;
    requireHeart: (isInFav: boolean) => string;
    onHeartHover: (isInFav: boolean) => (e: React.MouseEvent<HTMLImageElement>) => void;
    onHeartClick: (isInFav: boolean, joke: JokeDto) => () => void;
}

const SingleJoke: React.FC<Props> = (props: Props) => {
    const {joke, onSidebar, isInFav, requireHeart, onHeartHover, onHeartClick} = props;
    const [isMounted, setIsMounted] = useState(false);

    useEffect(() => {
        setIsMounted(true);
    }, []);

    return (
        <CSSTransition classNames="joke-animation"
            in={isMounted}
            timeout={300}
            unmountOnExit
        >
            <div className="joke"
                style={onSidebar ? {background: 'white'} : {opacity: 0.92}}
            >
                <img className="joke__save"
                    alt="Save as favourite"
                    src={requireHeart(isInFav)}
                    onMouseOver={onHeartHover(!isInFav)}
                    onMouseOut={onHeartHover(isInFav)}
                    onClick={onHeartClick(isInFav, joke)}
                />
                <div className="joke__message">
                    <div className="joke__message-picture-container"
                        style={onSidebar ? {background: '#F8F8F8'} : undefined}
                    >
                        <img className="joke__message-picture"
                            src={require('assets/img/message.svg')}
                            alt="Joke"
                        />
                    </div>
                    <div className="joke__content">
                        <div className="joke__id-link-container">
                            <span className="joke__id-link-text">ID: </span>
                            <a className="joke__id-link-text joke__id-link-text_color_blue"
                                target="_blank"
                                rel="noreferrer noopener"
                                href={joke.url}
                            >
                                {joke.id}
                            </a>
                            <img className="joke__id-link-pic" src={require('assets/img/link.svg')}
                                alt="link to joke"/>
                        </div>
                        <p className="joke__text">{joke.value}</p>
                        <p className="joke__date">Last update: 80 years ago</p>
                    </div>
                </div>
            </div>
        </CSSTransition>
    );
};

export default SingleJoke;
