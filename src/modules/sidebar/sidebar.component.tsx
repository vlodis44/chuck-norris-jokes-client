import React from 'react';
import {slide as SlideSidebar} from 'react-burger-menu';
import Jokes from 'modules/jokes/jokes.component';
import useWindowDimensions from '../../utils/use-window-dimensions.hook';

const WINDOW_WIDTH_BREAKPOINT = 1200;

const Sidebar: React.FC = () => {
    const {width} = useWindowDimensions();
    const isDesktop = width > WINDOW_WIDTH_BREAKPOINT;

    return (
        <SlideSidebar
            right
            isOpen={isDesktop}
            customBurgerIcon={isDesktop ? false :
                <img src={require('assets/img/burger.svg')} alt="Favourite"/>}
            customCrossIcon={isDesktop ? false :
                <img src={require('assets/img/cross.svg')} alt="Close menu"/>}
            noOverlay={isDesktop}
            disableAutoFocus
            disableCloseOnEsc
        >
            {isDesktop ? <p className="sidebar__fav-text">Favourite</p> : null}
            <div className="sidebar__jokes-container">
                <Jokes onSidebar/>
            </div>
        </SlideSidebar>
    );
};

export default Sidebar;
