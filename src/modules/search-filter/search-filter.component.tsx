import React, {useState} from 'react';
import API from 'utils/API';
import {useDispatch} from 'react-redux';
import {getJoke} from 'reducer';
import {JokeDto} from '../../dto/joke.dto';

const SearchFilter: React.FC = () => {
    const dispatch = useDispatch();
    const [selectedFilter, setSelectedFilter] = useState(1);
    const [selectedCategory, setSelectedCategory] = useState(-1);
    const [textInput, setTextInput] = useState('');

    const categories = ['animal', 'career', 'celebrity', 'dev', 'explicit', 'fashion', 'food', 'history', 'money',
        'movie', 'music', 'political', 'religion', 'science', 'sport', 'travel'];

    const onRadioClick = (i: number) => () => {
        setSelectedFilter(i);
        setSelectedCategory(-1);
    };

    const onCategoryClick = (i: number) => () => setSelectedCategory(i);

    const onInputChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        setTextInput(e.target.value);
    };

    const onSubmitClick = async () => {
        let jokeLink = '/';

        switch (selectedFilter) {
            case 1: {
                jokeLink += 'random';
                break;
            }
            case 2: {
                jokeLink += `random?category=${categories[selectedCategory]}`;
                break;
            }
            case 3: {
                jokeLink += `search?query=${textInput}`;
                break;
            }
        }

        const response = (await API.get(jokeLink)).data;

        if (response.result === undefined) {
            dispatch(getJoke(response));
        } else {
            response.result.forEach((jokeObj: JokeDto) => dispatch(getJoke(jokeObj)));
        }
    };

    const categoriesDiv = categories.map((category, i) => (
        <div
            key={i}
            className="category search-filter__category"
            style={selectedCategory === i ? {background: '#F8F8F8'} : undefined}
            onClick={onCategoryClick(i)}
        >
            <p
                className="category__category-text search-filter__category-text"
                style={selectedCategory === i ? {color: 'black'} : undefined}
            >
                {category}
            </p>
        </div>
    ));

    return (
        <>
            <div className="search-filter">
                <label className="search-filter__filter-label">
                    <input
                        className="search-filter__input-radio"
                        type="radio"
                        checked={selectedFilter === 1}
                        onChange={onRadioClick(1)}
                        value="random"
                        name="search-filter"
                    />
                    <span className="search-filter__input-radio_custom"/>
                    <span className="search-filter__filter-label-text">Random</span>
                </label>
                <label className="search-filter__filter-label">
                    <input
                        className="search-filter__input-radio"
                        type="radio"
                        checked={selectedFilter === 2}
                        onChange={onRadioClick(2)}
                        value="category"
                        name="search-filter"
                    />
                    <span className="search-filter__input-radio_custom"/>
                    <span className="search-filter__filter-label-text">From category</span>
                </label>
                <div
                    className="search-filter__categories-container"
                    style={selectedFilter === 2 ? {display: 'flex'} : undefined}
                >
                    {categoriesDiv}
                </div>
                <label className="search-filter__filter-label">
                    <input
                        className="search-filter__input-radio"
                        type="radio"
                        checked={selectedFilter === 3}
                        onChange={onRadioClick(3)}
                        value="text"
                        name="search-filter"
                    />
                    <span className="search-filter__input-radio_custom"/>
                    <span className="search-filter__filter-label-text">Text search</span>
                </label>
                <input
                    className="search-filter__text-input"
                    type="text"
                    value={textInput}
                    placeholder="Free text search..."
                    onChange={onInputChange}
                    style={selectedFilter === 3 ? {display: 'inline-block'} : undefined}
                />
                <button
                    type="submit"
                    className="search-filter__submit-button"
                    onClick={onSubmitClick}
                >
                    Get a joke
                </button>
            </div>
        </>
    );
};

export default SearchFilter;
