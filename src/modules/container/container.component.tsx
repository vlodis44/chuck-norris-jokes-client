import React from 'react';
import SearchFilter from 'modules/search-filter/search-filter.component';
import Jokes from 'modules/jokes/jokes.component';

const Container: React.FC = () => {
    return (
        <div className="wrapper">
            <div className="container wrapper__container">
                <div className="container__header">
                    <div className="container__greetings">
                        <p className="container__greetings_hey">Hey!</p>
                        <p className="container__greetings_lets">Let’s try to find a joke for you</p>
                    </div>
                    <div className="container__search-filter">
                        <SearchFilter/>
                    </div>
                </div>
                <div className="container__jokes">
                    <Jokes/>
                </div>
            </div>
        </div>
    );
};

export default Container;
