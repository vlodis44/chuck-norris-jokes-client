import React from 'react';
import Sidebar from 'modules/sidebar/sidebar.component';
import Container from 'modules/container/container.component';
import {useDispatch} from 'react-redux';
import {getFavsFromLocalStorage} from 'reducer';

function App() {
    const dispatch = useDispatch();
    dispatch(getFavsFromLocalStorage());

    return (
        <>
            <Sidebar/>
            <Container/>
        </>
    );
}

export default App;
