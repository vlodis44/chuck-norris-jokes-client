export interface JokeDto {
    'icon_url': string;
    'id': string;
    'url': string;
    'value': string;
    'categories': Array<string>;
}
