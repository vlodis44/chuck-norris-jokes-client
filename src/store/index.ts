import {configureStore} from '@reduxjs/toolkit';
import reducer from 'reducer';

export type RootState = ReturnType<typeof reducer>

const store = configureStore({
    reducer
});

export default store;
