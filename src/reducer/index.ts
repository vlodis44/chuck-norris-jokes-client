import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import {JokeDto} from '../dto/joke.dto';

interface StateType {
    receivedJokes: Array<JokeDto>;
    favouriteJokes: Array<JokeDto>;
}

const INIT_STATE: StateType = {
    receivedJokes: [],
    favouriteJokes: []
};

const jokesSlice = createSlice({
    name: 'jokes',
    initialState: INIT_STATE,
    reducers: {
        getJoke: (state, action: PayloadAction<JokeDto>) => {
            state.receivedJokes.push(action.payload);
        },

        getFavsFromLocalStorage: state => {
            state.favouriteJokes = Object.values(localStorage).map(obj => JSON.parse(obj));
        },

        addToFav: (state, action: PayloadAction<JokeDto>) => {
            state.favouriteJokes.push(action.payload);
            localStorage.setItem(action.payload.id, JSON.stringify(action.payload));
        },

        removeFromFav: (state, action: PayloadAction<JokeDto>) => {
            state.favouriteJokes = state.favouriteJokes.filter(joke => joke.id !== action.payload.id);
            localStorage.removeItem(action.payload.id);
        }
    }
});

const {actions, reducer} = jokesSlice;

export const {
    getJoke,
    getFavsFromLocalStorage,
    addToFav,
    removeFromFav
} = actions;

export default reducer;
